import express from 'express'
import { getUsers, register, login } from '../controllers/UsersController.js'
import { getCourier, addCourier, getOneCourier } from '../controllers/CouriersController.js'

const router = express.Router()

router.get('/users', getUsers)
router.post('/users', register)
router.post('/login', login)
// couriers
router.get('/couriers', getCourier)
router.get('/couriers/details/:origin_name/:destination_name', getOneCourier)
router.post('/couriers', addCourier)

export default router
import Users from '../models/UserModel.js'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

export const getUsers = async(req, res) => {
  try {
    const users = await Users.findAll()
    res.json(users)
  } catch(err){
    console.log(err)
  }
}

export const register = async(req, res) => {
  const { name, email, msisdn, password } = req.body
  const salt = await bcrypt.genSalt()
  const hashPassword = await bcrypt.hash(password, salt)
  try{
    await Users.create({
      name: name,
      email: email,
      msisdn: msisdn,
      password: hashPassword,
    })
    res.json({msg: 'Register Success'})
  } catch(err){
    console.log(err)
  }
}

export const login = async(req, res) => {
  try {
    const user = await Users.findAll({
      where: {
        msisdn: req.body.msisdn
      }
    })
    const match = await bcrypt.compare(req.body.password, user[0].password)
    if(!match) {
      return res.status(400).json({msg: "Wrong password"})
    }
    console.log(user)
    const userId = user[0].id
    const name = user[0].name
    const email = user[0].email
    const msisdn = user[0].msisdn
    const accessToken = jwt.sign({userId, name, email, msisdn}, process.env.ACCESS_TOKEN_SECRET, {
      expiresIn: '20s'
    })
    const refreshToken = jwt.sign({userId, name, email, msisdn}, process.env.REFRESH_TOKEN_SECRET, {
      expiresIn: '1d'
    })
    await Users.update({refresh_token: refreshToken}, {
      where: {
        id: userId
      }
    })
    res.cookie('refreshToken', refreshToken, {
      httpOnly: true,
      maxAge: 24 * 60 * 60 * 1000
    })
    res.json({ accessToken })
  } catch (err){
    res.status(404).json({msg: "Phone number not found"})
  }
}
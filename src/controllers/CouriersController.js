import Couriers from '../models/CourierModel.js'

export const getCourier = async(req, res) => {
  try {
    const couriers = await Couriers.findAll()
    res.json(couriers)
  } catch(err){
    console.log(err)
  }
}

export const getOneCourier = async(req, res) => {
  try {
    const couriers = await Couriers.findOne({
      where: {
        origin_name: req.body.origin_name,
        destination_name: req.body.destination_name,
      }
    })
    res.json(couriers)
  } catch(err){
    console.log(err)
  }
}

export const addCourier = async(req, res) => {
  const { logistic_name, amount, destination_name, origin_name, duration } = req.body
  try{
    await Couriers.create({
      logistic_name: logistic_name,
      amount: amount,
      destination_name: destination_name,
      origin_name: origin_name,
      duration: duration,
    })
    res.json({msg: 'add data success'})
  } catch(err){
    console.log(err)
  }
}

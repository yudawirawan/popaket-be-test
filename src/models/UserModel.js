import {Sequelize} from "sequelize"
import db from "../../config/Database.js"

const {DataTypes} = Sequelize;

const Users = db.define('users', {
  name: {
    type: DataTypes.STRING(100)
  },
  email: {
    type: DataTypes.STRING(50)
  },
  msisdn: {
    type: DataTypes.STRING(20)
  },
  password: {
    type: DataTypes.STRING(16)
  },
  refresh_token: {
    type: DataTypes.TEXT
  }
},{
  tableName: 'users',
  freezeTable: true,
})

export default Users
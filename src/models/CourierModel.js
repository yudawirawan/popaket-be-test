import {Sequelize} from "sequelize"
import db from "../../config/Database.js"

const {DataTypes} = Sequelize;

const Couriers = db.define('couriers', {
  logistic_name: {
    type: DataTypes.STRING(50)
  },
  amount: {
    type: DataTypes.INTEGER
  },
  destination_name: {
    type: DataTypes.STRING(50)
  },
  origin_name: {
    type: DataTypes.STRING(50)
  },
  duration: {
    type: DataTypes.STRING(5)
  }
},{
  tableName: 'couriers',
  freezeTable: true,
})

export default Couriers
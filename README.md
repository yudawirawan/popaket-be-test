## Cara install:
1. Clone project
2. masuk ke direktori project, lalu ketikan di terminal "npm install"
3. jalankan xampp untuk db mysql
4. import database "popaket-be-test.sql" (nama database bisa sama, atau menyesuaikan)
5. untuk menjalankan ketikan "npm run dev"


## Postman Share
https://go.postman.co/workspace/My-Workspace~456e44e5-5a77-4129-8e00-8f9e73fa599b/collection/18412390-336fcf6b-019d-4246-89db-bd78fec6fe1e?action=share&creator=18412390

## Postman Documentation
https://documenter.getpostman.com/view/18412390/UVyoUxNa

## note
- jwt login masih eror pada saat memasukan password, meskipun password benilai true
- swagger hanya bisa method get, untuk post hanya dapat berjalan menggunakan postman
- get data kurir sesuai dengan request, dan jika data ada di database
- db harus import, karena tidak memakai migration
import express from 'express'
import dotenv from 'dotenv'
import db from './config/Database.js'
import router from './src/routes/index.js'
import swaggerJsDoc from 'swagger-jsdoc'
import swaggerUI from 'swagger-ui-express'
import cors from 'cors'

dotenv.config()

const app = express()
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      tittle: 'Library API',
      version: '1.0.0',
    }
  },
  apis: ['app.js'],
}
const swaggerDocs = swaggerJsDoc(swaggerOptions)

try{
  await db.authenticate()
  console.log('Connected to database...')
}catch(err){
  console.log(err)
}

// Swagger UI

/**
 * @swagger
 * /users:
 *  get:
 *    description: Get All books
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /users:
 *  post:
 *    description: add data
 *    parameters:
 *    - name: name
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      in: formData
 *      required: true
 *      type: string
 *    - name: msisdn
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      in: formData
 *      required: true
 *      type: string
 *    - name: confPassword
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      201:
 *        description: Created
 */

/**
 * @swagger
 * /couriers:
 *  get:
 *    description: Get All couriers
 *    responses:
 *      200:
 *        description: Success
 * 
 */



/**
 * @swagger
 * /couriers:
 *  post:
 *    description: add data
 *    requestBody:
 *    content:
 *    aplication/json:
 *    parameters:
 *    - in: body
 *      required: true
 *      type: string
 *    responses:
 *      201:
 *        description: Created
 */


// end Swagger UI

app.use(express.json())
app.use(router)
app.use(cors())
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs))

app.listen(5000, ()=> console.log('server running at port 5000'))
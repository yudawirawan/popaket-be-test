-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2022 at 06:42 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `popaket-be-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

CREATE TABLE `couriers` (
  `id` int(11) NOT NULL,
  `logistic_name` varchar(50) NOT NULL,
  `amount` float NOT NULL,
  `destination_name` varchar(50) NOT NULL,
  `origin_name` varchar(50) NOT NULL,
  `duration` varchar(5) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `couriers`
--

INSERT INTO `couriers` (`id`, `logistic_name`, `amount`, `destination_name`, `origin_name`, `duration`, `createdAt`, `updatedAt`) VALUES
(1, 'JNE', 10000, 'Jakarta', 'Bandung', '2-4', '2022-03-20 12:03:28', '2022-03-20 12:03:28'),
(2, 'JNE', 10000, 'Bandung', 'Jakarta', '2-4', '2022-03-20 12:03:28', '2022-03-20 12:03:28'),
(3, 'Tiki', 12000, 'Yogyakarta', 'Bandung', '2-4', '2022-03-20 05:12:03', '2022-03-20 05:12:03'),
(4, 'Sicepat', 9000, 'Jakarta', 'Cianjur', '2-4', '2022-03-20 05:38:39', '2022-03-20 05:38:39');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `password` varchar(16) DEFAULT NULL,
  `refresh_token` text DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `msisdn`, `password`, `refresh_token`, `createdAt`, `updatedAt`) VALUES
(1, 'tes1111', 'tes1@gmail.com', '621111', '$2b$10$7.M6M5RsP', NULL, '2022-03-19 07:03:25', '2022-03-19 07:03:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `couriers`
--
ALTER TABLE `couriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `couriers`
--
ALTER TABLE `couriers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
